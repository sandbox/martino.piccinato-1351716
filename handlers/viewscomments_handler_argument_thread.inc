<?php
/**
 * Argument handler to accept a comment id to retrieve comments
 * belonging to a same thread.
 */
class viewscomments_handler_argument_thread extends views_handler_argument
{

    function default_actions($which = NULL)
    {
        // Disallow summary handlers on this argument.
        if (!$which) {
            $actions = parent::default_actions();
            unset($actions['summary asc']);
            unset($actions['summary desc']);
            return $actions;
        }

        if ($which != 'summary asc' && $which != 'summary desc') {
            return parent::default_actions($which);
        }
    }

    function query()
    {
        $this->ensure_my_table();
        $thread = viewscomments_get_comment_thread($this->argument);
        $this->query->add_where(0, "$this->table_alias.$this->real_field LIKE '%s%%'", substr($thread, 0, -1));
    }

}


