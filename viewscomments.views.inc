<?php

/**
 * Implementation of hook_views_data_alter
 */
function viewscomments_views_data_alter(&$data)
{

    $data['comments']['thread']['argument'] = array('handler' => 'viewscomments_handler_argument_thread',
                                                    'parent' => 'views_handler_argument',
                                                    'field' => 'thread',
                                                    'numeric' => TRUE,
                                                    'validate type' => 'cid',
                                                    'title' => t('Thread'),
                                                    'help' => t('The comment ID of the comment starting the thread.'));


}

/**
 * Implementation of hook_views_handlers
 */
function viewscomments_views_handlers()
{

    return array(
        'info' => array(
            'path' => drupal_get_path('module', 'viewscomments') . '/handlers',
        ),
        'handlers' => array(
            'viewscomments_handler_argument_thread' => array(
                'parent' => 'views_handler_argument',
            ),
        ),
    );

}

/**
 * Implementation of hook_views_plugins
 */
function viewscomments_views_plugins()
{
    return array(
        'argument validator' => array(
            'comment' => array(
                'title' => t('Comment CID'),
                'handler' => 'viewscomments_argument_validate_cid',
                'path' => drupal_get_path('module', 'viewscomments') . '/plugins',
            ),
        ),
    );
}