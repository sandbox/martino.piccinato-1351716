<?php
/**
 * @file
 * Contains the comment cid argument validator plugin.
 */

/**
 * Validate whether an argument is a valid comment cid or not
 *
 * @ingroup views_argument_validate_plugins
 */
class views_comments_thread_argument_validate_cid extends views_plugin_argument_validate
{
    var $option_name = 'cid';

    function validate_argument($argument)
    {
        return views_comments_thread_get_comment_thread($argument) ? true : false;
    }
}

